// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import Toasted from 'vue-toasted'

import App from './App'
import router from './router'
import store from './store'

import 'vuetify/dist/vuetify.min.css'

Vue.config.productionTip = false

Vue.use(Vuetify)
Vue.use(Toasted)

const mixin = {
  methods: {
    toastError (msg) {
      let text = ''

      console.log(msg)
      if (typeof msg === 'string') {
        switch (msg) {
          case 'input does not match any value of profile':
            text = 'Plan no encontrado en Winbox'
            break
          default:
            text = msg
            break
        }
      } else if (typeof msg === 'object') {
        if (msg.hasOwnProperty('response') &&
            msg.response.hasOwnProperty('data') &&
            msg.response.data.hasOwnProperty('message')) {
          // Show the message sent from the server.
          if (msg.response.data.hasOwnProperty('raw') &&
              typeof msg.response.data.raw === 'string') {
            switch (msg.response.data.raw) {
              case 'input does not match any value of profile':
                text = 'Plan no encontrado en Winbox'
                break
              default:
                text = msg.response.data.raw
                break
            }
          } else {
            text = msg.response.data.message
          }
        } else {
          text = 'Ha ocurrido un error en el servidor'
        }
      } else {
        text = 'Ha ocurrido un error en el proceso'
      }

      this.$toasted.show(text, {
        theme: 'bubble',
        duration: 3000,
        position: 'bottom-right'
      })
    }
  }
}

Vue.mixin(mixin)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
