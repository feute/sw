import Vue from 'vue'
import Router from 'vue-router'
// import Home from '@/pages/Home'
import AdminHome from '@/pages/AdminHome'
import AdminLogin from '@/pages/AdminLogin'
import AdminEmployeesList from '@/pages/AdminEmployeesList'
import AdminEmployeesDetails from '@/pages/AdminEmployeesDetails'
import AdminEmployeesAdd from '@/pages/AdminEmployeesAdd'
import AdminProvidersList from '@/pages/AdminProvidersList'
import AdminProvidersAdd from '@/pages/AdminProvidersAdd'
import AdminProvidersDetails from '@/pages/AdminProvidersDetails'
import AdminTransactionsList from '@/pages/AdminTransactionsList'
import AdminTransactionsAdd from '@/pages/AdminTransactionsAdd'
import AdminTransactionsDetails from '@/pages/AdminTransactionsDetails'
import AdminClientsAdd from '@/pages/AdminClientsAdd'
import AdminClientsList from '@/pages/AdminClientsList'
import AdminClientsDetails from '@/pages/AdminClientsDetails'
import AdminClientsEdit from '@/pages/AdminClientsEdit'
import AdminServicesAdd from '@/pages/AdminServicesAdd'
import AdminServicesEdit from '@/pages/AdminServicesEdit'
import AdminNodesAdd from '@/pages/AdminNodesAdd'
import AdminNodesList from '@/pages/AdminNodesList'
import AdminPlansAdd from '@/pages/AdminPlansAdd'
import AdminPlansList from '@/pages/AdminPlansList'
import AdminPlansEdit from '@/pages/AdminPlansEdit'
import AdminPlansTypesAdd from '@/pages/AdminPlansTypesAdd'
import AdminPlansTypesEdit from '@/pages/AdminPlansTypesEdit'
import LayoutAdmin from '@/components/LayoutAdmin'
// import LayoutBasic from '@/components/LayoutBasic'
import store from '@/store'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    // {
    //   path: '/',
    //   name: 'LayoutBasic',
    //   component: LayoutBasic,
    //   children: [
    //     {
    //       path: 'inicio',
    //       alias: '',
    //       component: Home,
    //       name: 'PageHome',
    //       meta: { description: 'Página de inicio' }
    //     }
    //   ]
    // },
    {
      path: '/',
      name: 'layout-admin',
      component: LayoutAdmin,
      beforeEnter (to, from, next) {
        if (!store.getters.loggedIn) {
          next({ name: 'admin-login' })
          return
        }
        next()
      },
      children: [
        {
          path: 'panel',
          alias: '',
          component: AdminHome,
          name: 'admin-home'
        },
        {
          path: 'empleados',
          component: AdminEmployeesList,
          name: 'employees-list'
        },
        {
          path: 'empleados/agregar',
          component: AdminEmployeesAdd,
          name: 'employees-add'
        },
        {
          path: 'empleados/:id',
          component: AdminEmployeesDetails,
          name: 'employees-details',
          props: true
        },
        {
          path: 'proveedores',
          component: AdminProvidersList,
          name: 'providers-list'
        },
        {
          path: 'proveedores/agregar',
          component: AdminProvidersAdd,
          name: 'providers-add'
        },
        {
          path: 'proveedores/:id',
          component: AdminProvidersDetails,
          name: 'providers-details',
          props: true
        },
        {
          path: 'movimientos',
          component: AdminTransactionsList,
          name: 'transactions-list'
        },
        {
          path: 'movimientos/agregar',
          component: AdminTransactionsAdd,
          name: 'transactions-add'
        },
        {
          path: 'movimientos/:id',
          component: AdminTransactionsDetails,
          name: 'transactions-details',
          props: true
        },
        {
          path: 'clientes',
          component: AdminClientsList,
          name: 'clients-list'
        },
        {
          path: 'clientes/agregar',
          component: AdminClientsAdd,
          name: 'clients-add'
        },
        {
          path: 'clientes/:id',
          component: AdminClientsDetails,
          name: 'clients-details',
          props: true
        },
        {
          path: 'clientes/:id/editar',
          component: AdminClientsEdit,
          name: 'clients-edit',
          props: true
        },
        {
          path: 'servicios/:id/agregar',
          component: AdminServicesAdd,
          name: 'services-add',
          props: true
        },
        {
          path: 'servicios/:id/editar',
          component: AdminServicesEdit,
          name: 'services-edit',
          props: true
        },
        {
          path: 'nodos/agregar',
          component: AdminNodesAdd,
          name: 'nodes-add'
        },
        {
          path: 'nodos',
          component: AdminNodesList,
          name: 'nodes-list'
        },
        {
          path: 'planes/agregar',
          component: AdminPlansAdd,
          name: 'plans-add'
        },
        {
          path: 'planes',
          component: AdminPlansList,
          name: 'plans-list'
        },
        {
          path: 'planes/:id/editar',
          component: AdminPlansEdit,
          name: 'plans-edit',
          props: true
        },
        {
          path: 'tipos-planes/:id/editar',
          component: AdminPlansTypesEdit,
          name: 'plans-types-edit',
          props: true
        },
        {
          path: 'tipos-planes/agregar',
          component: AdminPlansTypesAdd,
          name: 'plans-types-add'
        }
      ]
    },
    {
      path: '/entrar',
      name: 'admin-login',
      component: AdminLogin,
      beforeEnter (to, from, next) {
        if (store.getters.loggedIn) {
          next({ name: 'admin-home' })
          return
        }
        next()
      }
    }
  ]
})
