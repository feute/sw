import Vue from 'vue'
import Vuex from 'vuex'
import jwtDecode from 'jwt-decode'

import AuthService from '@/services/auth-service'
import axios from '@/services/api'

Vue.use(Vuex)

const getToken = (token) => {
  return token ? token.split(' ')[1] : null
}
// Local storage token.
const lsToken = localStorage.getItem('id_token')

export default new Vuex.Store({
  state: {
    token: lsToken || null,
    user: lsToken ? jwtDecode(lsToken).data : null
  },
  getters: {
    loggedIn (state) {
      return state.token !== null
    },
    user (state) {
      return state.user
    }
  },
  mutations: {
    retrieveToken (state, token) {
      state.token = token
      state.user = jwtDecode(token).data
    },

    destroyToken (state) {
      state.token = null
      state.user = null
    }
  },
  actions: {
    retrieveToken ({ commit }, credentials) {
      return new Promise(async (resolve, reject) => {
        try {
          const { data } = await AuthService.login(credentials)
          const token = getToken(data.token)
          localStorage.setItem('id_token', token)
          axios().defaults.headers.common['Authorization'] = `Bearer ${token}`
          commit('retrieveToken', token)

          resolve()
        } catch ({ response }) {
          // let message = ''

          // if (response.data.hasOwnProperty('message')) {
          //   message = response.data.message
          // } else {
          //   message = 'Error en el servidor'
          // }

          // Toast.open({
          //   queue: false,
          //   duration: 5000,
          //   message,
          //   type: 'is-danger'
          // })

          reject(response.data)
        }
      })
    },

    destroyToken ({ commit, getters }) {
      if (getters.loggedIn) {
        return new Promise((resolve, reject) => {
          localStorage.removeItem('id_token')
          axios().defaults.headers.common['Authorization'] = ''
          commit('destroyToken')
          resolve()
        })
      }
    }
  }
})
