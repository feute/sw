import api from '@/services/api'

export default {
  add (data) {
    return api().post('plans', data)
  },

  getAll () {
    return api().get('plans')
  },

  get (id) {
    return api().get(`plans/${id}`)
  },

  edit (id, data) {
    return api().patch(`plans/${id}`, data)
  },

  delete (id) {
    return api().delete(`plans/${id}`)
  }
}
