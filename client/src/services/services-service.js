import api from '@/services/api'

export default {
  add (data) {
    return api().post('services', data)
  },

  getAll () {
    return api().get('services')
  },

  get (id) {
    return api().get(`services/${id}`)
  },

  edit (id, data) {
    return api().patch(`services/${id}`, data)
  },

  delete (id) {
    return api().delete(`services/${id}`)
  }
}
