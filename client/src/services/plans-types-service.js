import api from '@/services/api'

export default {
  add (data) {
    return api().post('plans-types', data)
  },

  getAll () {
    return api().get('plans-types')
  },

  get (id) {
    return api().get(`plans-types/${id}`)
  },

  edit (id, data) {
    return api().patch(`plans-types/${id}`, data)
  },

  delete (id) {
    return api().delete(`plans-types/${id}`)
  }
}
