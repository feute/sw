import api from '@/services/api'

export default {
  add (data) {
    return api().post('clients', data)
  },

  getAll () {
    return api().get('clients')
  },

  get (id) {
    return api().get(`clients/${id}`)
  },

  edit (id, data) {
    return api().patch(`clients/${id}`, data)
  }
}
