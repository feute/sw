import api from '@/services/api'

export default {
  add (data) {
    return api().post('employees', data)
  },

  getAll () {
    return api().get('employees')
  },

  get (id) {
    return api().get(`employees/${id}`)
  }
}
