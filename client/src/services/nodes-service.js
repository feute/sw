import api from '@/services/api'

export default {
  add (data) {
    return api().post('nodes', data)
  },

  getAll () {
    return api().get('nodes')
  },

  get (id) {
    return api().get(`nodes/${id}`)
  },

  getInfo (id) {
    return api().get(`nodes/${id}/info`)
  }
}
