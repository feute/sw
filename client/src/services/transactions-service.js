import api from '@/services/api'

export default {
  add (data) {
    return api().post('transactions', data)
  },

  getAll () {
    return api().get('transactions')
  },

  get (id) {
    return api().get(`transactions/${id}`)
  }
}
