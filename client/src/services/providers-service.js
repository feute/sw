import api from '@/services/api'

export default {
  add (data) {
    return api().post('providers', data)
  },

  getAll () {
    return api().get('providers')
  },

  get (id) {
    return api().get(`providers/${id}`)
  }
}
