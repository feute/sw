const bookshelf = require('../db')

module.exports = bookshelf.model('Client', {
  tableName: 'clientes',
  servicios: function () {
    return this.hasMany('Service', 'id_cliente')
  },
  nodo: function () {
    return this.belongsTo('Node', 'id_nodo')
  },
  movimientos: function () {
    return this.hasMany('Transaction', 'id_cliente')
  }
})
