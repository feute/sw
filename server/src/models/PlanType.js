const bookshelf = require('../db')

module.exports = bookshelf.model('PlanType', {
  tableName: 'tipos_planes',
  planes: function () {
    return this.hasMany('Plan', 'id_tipo')
  }
})
