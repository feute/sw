const bookshelf = require('../db')

module.exports = bookshelf.model('Provider', {
  tableName: 'proveedores',
  servicios: function () {
    return this.hasMany('Service', 'id_proveedor')
  }
})
