const bookshelf = require('../db')

module.exports = bookshelf.model('Transaction', {
  tableName: 'movimientos',
  cliente: function () {
    return this.belongsTo('Client', 'id_cliente')
  },
  detalles: function () {
    return this.hasMany('TransactionDetail', 'id_movimiento')
  }
})
