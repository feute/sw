const bookshelf = require('../db')

module.exports = bookshelf.model('Plan', {
  tableName: 'planes',
  tipo: function () {
    return this.belongsTo('PlanType', 'id_tipo')
  },
  servicios: function () {
    return this.hasMany('Service', 'id_plan')
  }
})
