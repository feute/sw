const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt'))

const config = require('../config')
const bookshelf = require('../db')

module.exports = bookshelf.model('Employee', {
  tableName: 'empleados',
  initialize () {
    this.on('saving', (model) => {
      if (!model.hasChanged('clave')) {
        return
      }

      return Promise.coroutine(function* () {
        const salt = yield bcrypt.genSaltAsync(config.saltRounds)
        const hashedPassword = yield bcrypt.hashAsync(
          model.attributes.clave,
          salt
        )

        model.set('clave', hashedPassword)
      })()
    })
  },
  validPassword (password) {
    return bcrypt.compareAsync(password, this.attributes.clave)
  }
})
