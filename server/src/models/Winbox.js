const bookshelf = require('../db')

module.exports = bookshelf.model('Winbox', {
  tableName: 'winbox',
  cliente: function () {
    return this.belongsTo('Client', 'id_cliente')
  }
})
