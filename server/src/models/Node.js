const bookshelf = require('../db')

module.exports = bookshelf.model('Node', {
  tableName: 'nodos',
  clientes: function () {
    return this.hasMany('Client', 'id_nodo')
  },
  servicios: function () {
    return this.hasMany('Service', 'id_nodo')
  }
})
