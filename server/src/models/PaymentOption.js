const bookshelf = require('../db')

module.exports = bookshelf.model('PaymentOption', {
  tableName: 'formas_pago'
})
