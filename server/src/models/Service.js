const bookshelf = require('../db')

module.exports = bookshelf.model('Service', {
  tableName: 'servicios',
  nodo: function () {
    return this.belongsTo('Node', 'id_nodo')
  },
  cliente: function () {
    return this.belongsTo('Client', 'id_cliente')
  },
  plan: function () {
    return this.belongsTo('Plan', 'id_plan')
  }
})
