const bookshelf = require('../db')

module.exports = bookshelf.model('Category', {
  tableName: 'categorias'
})
