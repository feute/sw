const bookshelf = require('../db')

module.exports = bookshelf.model('TransactionDetail', {
  tableName: 'detalles_movimientos',
  movimiento: function () {
    return this.belongsTo('Transaction', 'id_movimiento')
  }
})
