const MikroNode = require('mikronode')

const Node = require('../models/Node')

module.exports = {
  /**
   * Map through the `data` array and return the `value` property of
   * the object matching the `field` argument; which is compared against
   * the key of the same name, `field`.
   *
   * @param {Array} data - Array of objects in the form field/value.
   * @param {string} field
   * @return {string} Field value if found in the object. Otherwise, an
   * empty string is returned.
   */
  getFieldValue (data, field) {
    let value = ''
    if (typeof data === 'object' && data.length > 0) {
      data.map((item) => {
        if (item.hasOwnProperty('field') && item.field === field) {
          value = item.value
        }
      })
    }

    return value
  },

  isWinboxError (err) {
    if (typeof err === 'object' &&
        err.hasOwnProperty('data') &&
        typeof err.data === 'object') {
      return true
    }
    return false
  },

  getError (err) {
    let message = ''
    if (this.isWinboxError(err)) {
      err.data.map((data) => {
        if (data.field === 'message') {
          message = data.value
        }
        return data
      })
    } else {
      message = err.message
    }
    return message
  },

  /**
   * Run a command inside a RouterOS node.
   *
   * @param {string} ip - IP address of the node to be connected.
   * @param {string} channel - Channel name representing the command.
   * @param {string} command - Command in the form `/ip/hotspot/user`.
   * @param {Object} [args=[]] - Arguments list passed to the command.
   * @return {Promise}
   */
  runCommand (ip, channel, command, args = []) {
    return new Promise((resolve, reject) => {
      const device = new MikroNode(ip)

      device.connect()
        .then(([login]) => login(process.env.WINBOX_USER, process.env.WINBOX_PASS))
        .then((conn) => {
          conn.closeOnDone(true)

          const chan = conn.openChannel(channel)
          chan.closeOnDone(true)
          chan.write(command, args)
          chan.on('done', (data) => resolve(data))
          chan.on('trap', (data) => reject(data))
        })
        .catch((err) => reject(err))
    })
  },

  /**
   * Add a user to the RouterOS node.
   *
   * The node is retrieved from the client object (id_nodo), and its
   * properties (mainly the IP) is retrieved through Bookshelf.
   *
   * There is two arguments because they are retrieved from the frontend
   * as two objects (two different form scopes), and because it is saved
   * separately in the database.
   *
   * @param {Object} client - JSON object representing client's properties.
   * @param {number} client.id_nodo
   * @param {string} client.nombre
   * @param {string} client.correo1
   * @param {string} client.direccion
   * @param {string} client.plan
   * @param {Object} winbox - JSON object representing properties for RouterOS.
   * @param {string} winbox.name
   * @param {string} winbox.password
   * @param {string} winbox.server
   * @param {string} winbox.mac=00:00:00:00:00:00
   * @param {string} winbox.address=0.0.0.0
   * @return {Promise}
   */
  addUser (client, service) {
    const { nombre, correo1, direccion } = client
    const { id_nodo, name, password, server, mac, address, plan } = service

    let comment = nombre
    if (!!direccion && direccion !== '') {
      comment += `, ${direccion}`
    }

    return new Promise((resolve, reject) => {
      Node.forge({ id: id_nodo }).fetch({ required: true })
        .then((node) => {
          return this.runCommand(
            node.get('ip'),
            'addUser',
            '/ip/hotspot/user/add',
            {
              name,
              server: server || 'all',
              password,
              comment,
              email: correo1,
              profile: `${plan.tipo.prefijo} ${plan.ancho_banda}`,
              address: address || '0.0.0.0',
              'mac-address': mac || '00:00:00:00:00:00'
            }
          )
        })
        .then((data) => {
          resolve(data)
        })
        .catch((err) => {
          reject(err)
        })
    })
  },

  suspendUser (nodeIP, clientName) {
    return this.runCommand(
      nodeIP,
      'usersList',
      '/ip/hotspot/user/print'
    ).then((listData) => {
      const usersList = listData.data.filter((item) => {
        return this.getFieldValue(item, 'name') === clientName
      })
      if (usersList.length < 1) {
        throw new Error('Usuario no encontrado en Winbox')
      }

      const userID = this.getFieldValue(usersList[0], '.id')
      return this.runCommand(
        nodeIP,
        'removeUser',
        '/ip/hotspot/user/set',
        { '.id': userID, profile: 'Suspendido' }
      )
    })
  },

  activateUser (nodeIP, name, service) {
    return this.runCommand(
      nodeIP,
      'usersList',
      '/ip/hotspot/user/print'
    ).then((listData) => {
      const { plan } = service
      const usersList = listData.data.filter((item) => {
        return this.getFieldValue(item, 'name') === name
      })
      if (usersList.length < 1) {
        throw new Error('Usuario no encontrado en Winbox')
      }

      const userID = this.getFieldValue(usersList[0], '.id')
      return this.runCommand(
        nodeIP,
        'activateUser',
        '/ip/hotspot/user/set',
        { '.id': userID, profile: `${plan.tipo.prefijo} ${plan.ancho_banda}` }
      )
    })
  },

  removeUser (nodeIP, clientName) {
    return this.runCommand(
      nodeIP,
      'usersList',
      '/ip/hotspot/user/print'
    ).then((listData) => {
      const usersList = listData.data.filter((item) => {
        return this.getFieldValue(item, 'name') === clientName
      })
      if (usersList.length < 1) {
        throw new Error('Usuario no encontrado en Winbox')
      }

      const userID = this.getFieldValue(usersList[0], '.id')
      return this.runCommand(
        nodeIP,
        'removeUser',
        '/ip/hotspot/user/remove',
        { '.id': userID }
      )
    })
  },

  updateByService (nodeIP, oldName, service) {
    const { name, address, mac, password, plan, server } = service

    return this.runCommand(
      nodeIP,
      'usersList',
      '/ip/hotspot/user/print'
    )
      .then((listData) => {
        const usersList = listData.data.filter((item) => {
          return this.getFieldValue(item, 'name') === oldName
        })
        if (usersList.length < 1) {
          throw new Error('Usuario no encontrado en Winbox')
        }
        const userID = this.getFieldValue(usersList[0], '.id')
        return this.runCommand(
          nodeIP,
          'updateByService',
          '/ip/hotspot/user/set',
          {
            '.id': userID,
            name,
            server: server || 'all',
            password,
            profile: `${plan.tipo.prefijo} ${plan.ancho_banda}`,
            address: address || '0.0.0.0',
            'mac-address': mac || '00:00:00:00:00:00'
          }
        )
      })
  },

  updateByClient (nodeIP, name, client) {
    const { nombre, correo1, direccion } = client
    let comment = nombre
    if (!!direccion && direccion !== '') {
      comment += `, ${direccion}`
    }

    return this.runCommand(
      nodeIP,
      'usersList',
      '/ip/hotspot/user/print'
    )
      .then((listData) => {
        const usersList = listData.data.filter((item) => {
          return this.getFieldValue(item, 'name') === name
        })
        if (usersList.length < 1) {
          throw new Error('Usuario no encontrado en Winbox')
        }
        const userID = this.getFieldValue(usersList[0], '.id')
        return this.runCommand(
          nodeIP,
          'updateByClient',
          '/ip/hotspot/user/set',
          {
            '.id': userID,
            comment,
            email: correo1
          }
        )
      })
  }
}
