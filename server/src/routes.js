const AuthController = require('./controllers/auth-controller')
const EmployeeController = require('./controllers/employees-controller')
const ProvidersController = require('./controllers/providers-controller')
const NodesController = require('./controllers/nodes-controller')
const ServicesController = require('./controllers/services-controller')
const TransactionsController = require('./controllers/transactions-controller')
const ClientsController = require('./controllers/clients-controller')
const PlansController = require('./controllers/plans-controller')
const PlansTypesController = require('./controllers/plans-types-controller')

module.exports = (app) => {
  app.post('/login', AuthController.login)
  // --
  app.get('/employees', EmployeeController.getAll)
  app.get('/employees/:id', EmployeeController.get)
  app.post('/employees', EmployeeController.add)
  // --
  app.get('/providers', ProvidersController.getAll)
  app.get('/providers/:id', ProvidersController.get)
  app.post('/providers', ProvidersController.add)
  // --
  app.get('/nodes', NodesController.getAll)
  app.get('/nodes/:id', NodesController.get)
  app.get('/nodes/:id/info', NodesController.getInfo)
  app.post('/nodes', NodesController.add)
  // --
  app.get('/services', ServicesController.getAll)
  app.get('/services/:id', ServicesController.get)
  app.post('/services', ServicesController.add)
  app.patch('/services/:id', ServicesController.edit)
  app.delete('/services/:id', ServicesController.delete)
  // --
  app.get('/transactions', TransactionsController.getAll)
  app.get('/transactions/:id', TransactionsController.get)
  app.post('/transactions', TransactionsController.add)
  // --
  app.get('/clients', ClientsController.getAll)
  app.get('/clients/:id', ClientsController.get)
  app.patch('/clients/:id', ClientsController.edit)
  app.post('/clients', ClientsController.add)
  // --
  app.get('/plans', PlansController.getAll)
  app.get('/plans/:id', PlansController.get)
  app.patch('/plans/:id', PlansController.edit)
  app.post('/plans', PlansController.add)
  // --
  app.get('/plans-types', PlansTypesController.getAll)
  app.get('/plans-types/:id', PlansTypesController.get)
  app.patch('/plans-types/:id', PlansTypesController.edit)
  app.post('/plans-types', PlansTypesController.add)
}
