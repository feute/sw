const env = process.env.NODE_ENV || 'development'

const knexConfig = require('../knexfile')
const knex = require('knex')(knexConfig[env])

const bookshelf = require('bookshelf')(knex)

bookshelf.plugin('registry')

module.exports = bookshelf
