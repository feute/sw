const passport = require('passport')
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt

const config = require('./config')
const Employee = require('./models/Employee')

module.exports = () => {
  const opts = {}
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken()
  opts.secretOrKey = config.secret
  passport.use(new JwtStrategy(opts, async function (jwtPayload, done) {
    try {
      const employee = await Employee.where('id', jwtPayload.id).fetch()
      if (!employee) {
        done(null, false)
      } else {
        done(null, employee)
      }
    } catch (err) {
      done(err, false)
    }
  }))
}
