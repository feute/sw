const Client = require('../models/Client')
const Service = require('../models/Service')
const routeros = require('../utils/routeros')

module.exports = {
  async add (req, res) {
    let { client, services } = req.body
    console.log(req.body)

    try {
      // If there are any services, then add it to Winbox.
      if (services.length > 0) {
        services.forEach(async (service) => {
          if (service.status === 'Activo') {
            await routeros.addUser(client, service)
          }
        })

        const _client = await Client.forge(client).save()

        services.map(async (service) => {
          service.id_cliente = _client.get('id')
          await Service.forge(service).save()
          return service
        })

        res.status(201).send({
          client: _client.toJSON()
        })
      } else {
        const _client = await Client.forge(client).save()
        res.status(201).send(_client.toJSON())
      }
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo guardar el cliente en la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async get (req, res) {
    const { id } = req.params
    try {
      const client = await Client.forge({ id }).fetch({
        required: true,
        withRelated: ['servicios.nodo', 'servicios.plan.tipo']
      })
      res.send(client.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener el cliente de la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async getAll (req, res) {
    try {
      const clients = await Client.forge().fetchAll({
        withRelated: ['servicios.nodo', 'servicios.plan']
      })
      res.send(clients.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener la lista de clientes',
        raw: routeros.getError(err)
      })
    }
  },

  async edit (req, res) {
    const { id } = req.params
    try {
      let { client } = req.body
      const _client = await Client
        .forge({ id })
        .fetch({ required: true, withRelated: ['servicios.nodo'] })

      await _client.save(client, { patch: true })
      const clientObj = _client.toJSON()

      // Update info in each of client's services.
      clientObj.servicios.map(async (service) => {
        await routeros.updateByClient(service.nodo.ip, service.name, clientObj)
        return service
      })
      res.send(_client.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo editar el cliente',
        raw: routeros.getError(err)
      })
    }
  }
}
