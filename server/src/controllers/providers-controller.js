const Provider = require('../models/Provider')
const routeros = require('../utils/routeros')

module.exports = {
  async add (req, res) {
    try {
      const provider = await Provider.forge(req.body).save()
      res.status(201).send(provider.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo guardar el proveedor en la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async get (req, res) {
    const { id } = req.params
    try {
      const provider = await Provider.forge({ id }).fetch({ required: true })
      res.send(provider.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener el proveedor de la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async getAll (req, res) {
    try {
      const providers = await Provider.forge().fetchAll({ withRelated: ['servicios'] })
      res.send(providers.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener la lista de proveedores',
        raw: routeros.getError(err)
      })
    }
  }
}
