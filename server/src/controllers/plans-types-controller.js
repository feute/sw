require('../models/Plan')
const PlanType = require('../models/PlanType')
const routeros = require('../utils/routeros')

module.exports = {
  async add (req, res) {
    try {
      const type = await PlanType.forge(req.body.type).save()
      res.status(201).send(type.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo guardar el tipo de plan en la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async get (req, res) {
    const { id } = req.params
    try {
      const type = await PlanType.forge({ id }).fetch({ required: true, withRelated: ['planes'] })
      res.send(type.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener el plan de la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async getAll (req, res) {
    try {
      const type = await PlanType.forge().fetchAll({ withRelated: ['planes'] })
      res.send(type.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener los tipos de planes',
        raw: routeros.getError(err)
      })
    }
  },

  async edit (req, res) {
    const { id } = req.params
    try {
      let { type } = req.body
      const _type = await PlanType
        .forge({ id })
        .fetch({ required: true })

      await _type.save(type, { patch: true })
      res.send(_type.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo editar el tipo de plan',
        raw: routeros.getError(err)
      })
    }
  }
}
