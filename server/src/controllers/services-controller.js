const Service = require('../models/Service')
const routeros = require('../utils/routeros')

module.exports = {
  async add (req, res) {
    try {
      const { service } = req.body
      let _service = await Service.forge(service).load(['cliente', 'nodo', 'plan.tipo'])
      await routeros.addUser(
        _service.related('cliente').toJSON(),
        _service.toJSON()
      )
      await _service.save()
      res.status(201).send(_service.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo guardar el servicio en la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async get (req, res) {
    const { id } = req.params
    try {
      const service = await Service.forge({ id }).fetch({ required: true })
      res.send(service.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener el servicio de la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async getAll (req, res) {
    try {
      const services = await Service.forge().fetchAll({ withRelated: ['proveedor'] })
      res.send(services.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener la lista de servicios',
        raw: routeros.getError(err)
      })
    }
  },

  async edit (req, res) {
    const { id } = req.params
    try {
      let { service } = req.body
      const _service = await Service
        .forge({ id })
        .fetch({ required: true, withRelated: ['cliente', 'nodo', 'plan.tipo'] })

      const serviceObj = _service.toJSON()
      if (service.hasOwnProperty('status')) {
        // This status is handled via action buttons.
        switch (service.status) {
          case 'Activo':
            if (serviceObj.status === 'Retirado') {
              await routeros.addUser(
                _service.related('cliente').toJSON(),
                _service.toJSON()
              )
            } else {
              await routeros.activateUser(serviceObj.nodo.ip, serviceObj.name, serviceObj)
            }
            break
          case 'Suspendido':
            await routeros.suspendUser(
              _service.related('nodo').get('ip'),
              _service.get('name')
            )
            break
          case 'Retirado':
            await routeros.removeUser(
              _service.related('nodo').get('ip'),
              _service.get('name')
            )
            break
        }
      } else {
        // Update info in Winbox if the service is not retired.
        if (serviceObj.status !== 'Retirado') {
          await routeros.updateByService(serviceObj.nodo.ip, serviceObj.name, service)
        }
      }

      await _service.save(service, { patch: true })
      res.send(serviceObj)
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo editar el cliente',
        raw: routeros.getError(err)
      })
    }
  },

  async delete (req, res) {
    try {
      const { id } = req.params
      const service = await Service
        .forge({ id })
        .fetch({ required: true, withRelated: ['nodo'] })

      // Save service data before deleting.
      // After running `destroy`, the model gets cleaned up.
      const _service = service.toJSON()

      await service.destroy()
      await routeros.removeUser(
        _service.nodo.ip,
        _service.name
      )
      res.send(_service)
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo eliminar el servicio',
        raw: routeros.getError(err)
      })
    }
  }
}
