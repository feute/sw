const jwt = require('jsonwebtoken')

const config = require('../config')
const routeros = require('../utils/routeros')
const Employee = require('../models/Employee')

module.exports = {
  async login (req, res) {
    const { username, password } = req.body

    if (!username, !password) {
      res.status(400).send({ message: 'Fallo en la autenticación' })
      return
    }

    try {
      const employee = await Employee.where('usuario', username).fetch()
      if (!employee) {
        res.status(404).send({ message: 'Usuario no encontrado' })
        return
      }
      const isValidPassword = await employee.validPassword(password)

      if (isValidPassword) {
        const token = jwt.sign({
          data: employee.omit('clave')
        }, config.secret)
        res.send({ token: `Bearer ${token}` })
      } else {
        res.status(403).send({ message: 'Fallo en la autenticación' })
      }
    } catch (err) {
      console.error(err)
      res.status(500).send({ message: 'Error en el servidor', raw: routeros.getError(err) })
    }
  }
}
