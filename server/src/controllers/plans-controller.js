require('../models/PlanType')
const Plan = require('../models/Plan')
const routeros = require('../utils/routeros')

module.exports = {
  async add (req, res) {
    try {
      const plan = await Plan.forge(req.body.plan).save()
      res.status(201).send(plan.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo guardar el plan en la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async get (req, res) {
    const { id } = req.params
    try {
      const plan = await Plan.forge({ id }).fetch({ required: true, withRelated: ['tipo'] })
      res.send(plan.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener el plan de la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async getAll (req, res) {
    try {
      const plan = await Plan.forge().fetchAll({ withRelated: ['tipo'] })
      res.send(plan.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener la lista de planes',
        raw: routeros.getError(err)
      })
    }
  },

  async edit (req, res) {
    const { id } = req.params
    try {
      let { plan } = req.body
      const _plan = await Plan
        .forge({ id })
        .fetch({ required: true })

      await _plan.save(plan, { patch: true })
      res.send(_plan.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo editar el plan',
        raw: routeros.getError(err)
      })
    }
  }
}
