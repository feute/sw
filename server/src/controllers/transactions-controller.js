const bookshelf = require('../db')
const Transaction = require('../models/Transaction')
const TransactionDetail = require('../models/TransactionDetail')
const routeros = require('../utils/routeros')

module.exports = {
  async add (req, res) {
    const { transaction, details } = req.body
    try {
      const _transaction = await Transaction.forge(transaction).save()
      if (details.length > 0) {
        details.map((detail) => {
          detail.id_movimiento = _transaction.get('id')
          return detail
        })
        await bookshelf.knex('detalles_movimientos').insert(details)
      }
      res.status(201).send(_transaction.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo guardar el movimiento en la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async get (req, res) {
    const { id } = req.params
    try {
      const transaction = await Transaction.forge({ id }).fetch({
        required: true,
        withRelated: ['detalles.servicio', 'cliente']
      })
      res.send(transaction.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener el movimiento de la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async getAll (req, res) {
    try {
      const transactions = await Transaction.forge().fetchAll({
        withRelated: ['detalles.servicio', 'cliente']
      })
      res.send(transactions.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener la lista de movimientos',
        raw: routeros.getError(err)
      })
    }
  }
}
