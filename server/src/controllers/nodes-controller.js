const Node = require('../models/Node')
const routeros = require('../utils/routeros')

module.exports = {
  async add (req, res) {
    try {
      const node = await Node.forge(req.body).save()
      res.status(201).send(node.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo guardar el nodo en la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async get (req, res) {
    const { id } = req.params
    try {
      const node = await Node.forge({ id }).fetch({ required: true })
      res.send(node.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener el nodo de la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async getAll (req, res) {
    try {
      const nodes = await Node.forge().fetchAll()
      res.send(nodes.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener la lista de nodos',
        raw: routeros.getError(err)
      })
    }
  },

  async getInfo (req, res) {
    const { id } = req.params
    try {
      const node = await Node.forge({ id }).fetch({ required: true })
      const ip = node.get('ip')

      const servers = await routeros.runCommand(
        ip,
        'serversList',
        '/ip/hotspot/print'
      )
      // Filter the servers response to only include server names.
      const serversList = servers.data.map((item) => {
        return routeros.getFieldValue(item, 'name')
      })

      const profiles = await routeros.runCommand(
        ip,
        'profilesList',
        '/ip/hotspot/user/profile/print'
      )
      // Filter the profiles response to only include profiles names.
      const profilesList = profiles.data.map((item) => {
        return routeros.getFieldValue(item, 'name')
      })

      res.send({ servers: serversList, profiles: profilesList })
    } catch (err) {
      console.log(err)
      res.status(500).send({ message: 'Error', raw: routeros.getError(err) })
    }
  }
}
