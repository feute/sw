const Employee = require('../models/Employee')
const routeros = require('../utils/routeros')

module.exports = {
  async add (req, res) {
    try {
      const employee = await Employee.forge(req.body).save()
      res.status(201).send(employee.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo guardar el empleado en la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async get (req, res) {
    const { id } = req.params
    try {
      const employee = await Employee.forge({ id }).fetch({ required: true })
      res.send(employee.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener el empleado de la base de datos',
        raw: routeros.getError(err)
      })
    }
  },

  async getAll (req, res) {
    try {
      const employees = await Employee.forge().fetchAll()
      res.send(employees.toJSON())
    } catch (err) {
      console.log(err)
      res.status(500).send({
        message: 'No se pudo obtener la lista de empleados',
        raw: routeros.getError(err)
      })
    }
  }
}
