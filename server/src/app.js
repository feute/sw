const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const passport = require('passport')

const configurePassport = require('./passport')

require('dotenv').config()

const app = express()
const isProduction = process.env.NODE_ENV === 'production'

app.use(morgan(isProduction ? 'combined' : 'dev'))
app.use(passport.initialize())
configurePassport()
app.use(bodyParser.json())
app.use(cors())

require('./routes')(app)

app.listen(process.env.PORT || 8081)
