const bcrypt = require('bcrypt')

const config = require('../src/config')

const password = 'admin'

exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('empleados').del()
    .then(async function () {
      try {
        const salt = await bcrypt.genSalt(config.saltRounds)
        const hash = await bcrypt.hash(password, salt)
        // Inserts seed entries
        return knex('empleados').insert([
          {
            id: 1,
            usuario: 'admin',
            clave: hash,
            nombres: 'Admin'
          }
        ])
      } catch (err) {
        console.error(err)
      }
    })
}
