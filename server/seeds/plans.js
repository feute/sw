exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('planes').del()
    .then(function () { return knex('tipos_planes').del() })
    .then(function () {
      // Inserts seed entries
      return knex('tipos_planes').insert([
        { id: 1, nombre: 'Residencial Premium', prefijo: 'Res' },
        { id: 2, nombre: 'Residencial Nodos', prefijo: 'Res' },
        { id: 3, nombre: 'Residencial Lite', prefijo: 'Res' },
        { id: 4, nombre: 'Dedicado', prefijo: 'Ded' },
        { id: 5, nombre: 'Nateado', prefijo: 'Nat' },
        { id: 6, nombre: 'Comercial', prefijo: 'Com' }
      ])
    })
    .then(function () {
      // Inserts seed entries
      return knex('planes').insert([
        // Residenciales Premium
        { id_tipo: 1, ancho_banda: '1 Mbps', winbox: '', costo: 12.0 },
        { id_tipo: 1, ancho_banda: '2 Mbps', winbox: '', costo: 23.0 },
        { id_tipo: 1, ancho_banda: '3 Mbps', winbox: '', costo: 34.0 },
        { id_tipo: 1, ancho_banda: '4 Mbps', winbox: '', costo: 45.0 },
        { id_tipo: 1, ancho_banda: '5 Mbps', winbox: '', costo: 55.0 },
        { id_tipo: 1, ancho_banda: '6 Mbps', winbox: '', costo: 65.0 },
        { id_tipo: 1, ancho_banda: '7 Mbps', winbox: '', costo: 74.0 },
        { id_tipo: 1, ancho_banda: '8 Mbps', winbox: '', costo: 83.0 },
        { id_tipo: 1, ancho_banda: '9 Mbps', winbox: '', costo: 92.0 },
        { id_tipo: 1, ancho_banda: '10 Mbps', winbox: '', costo: 100.0 },
        // Residenciales Nodos
        { id_tipo: 2, ancho_banda: '1 Mbps', winbox: '', costo: 10.20 },
        { id_tipo: 2, ancho_banda: '2 Mbps', winbox: '', costo: 19.55 },
        { id_tipo: 2, ancho_banda: '3 Mbps', winbox: '', costo: 28.90 },
        { id_tipo: 2, ancho_banda: '4 Mbps', winbox: '', costo: 38.25 },
        { id_tipo: 2, ancho_banda: '5 Mbps', winbox: '', costo: 46.75 },
        { id_tipo: 2, ancho_banda: '6 Mbps', winbox: '', costo: 55.25 },
        { id_tipo: 2, ancho_banda: '7 Mbps', winbox: '', costo: 62.90 },
        { id_tipo: 2, ancho_banda: '8 Mbps', winbox: '', costo: 70.55 },
        { id_tipo: 2, ancho_banda: '9 Mbps', winbox: '', costo: 78.20 },
        { id_tipo: 2, ancho_banda: '10 Mbps', winbox: '', costo: 85.0 },
        // Residenciales Lite
        { id_tipo: 3, ancho_banda: '1 Mbps', winbox: '', costo: 8.67 },
        { id_tipo: 3, ancho_banda: '2 Mbps', winbox: '', costo: 16.62 },
        { id_tipo: 3, ancho_banda: '3 Mbps', winbox: '', costo: 24.57 },
        { id_tipo: 3, ancho_banda: '4 Mbps', winbox: '', costo: 32.51 },
        { id_tipo: 3, ancho_banda: '5 Mbps', winbox: '', costo: 39.74 },
        { id_tipo: 3, ancho_banda: '6 Mbps', winbox: '', costo: 46.96 },
        { id_tipo: 3, ancho_banda: '7 Mbps', winbox: '', costo: 53.47 },
        { id_tipo: 3, ancho_banda: '8 Mbps', winbox: '', costo: 59.97 },
        { id_tipo: 3, ancho_banda: '9 Mbps', winbox: '', costo: 66.47 },
        { id_tipo: 3, ancho_banda: '10 Mbps', winbox: '', costo: 72.25 },
        // Dedicado
        { id_tipo: 4, ancho_banda: '10 Mbps', winbox: '', costo: 580.80 },
        { id_tipo: 4, ancho_banda: '15 Mbps', winbox: '', costo: 871.20 },
        { id_tipo: 4, ancho_banda: '20 Mbps', winbox: '', costo: 1161.60 },
        { id_tipo: 4, ancho_banda: '25 Mbps', winbox: '', costo: 1452.0 },
        { id_tipo: 4, ancho_banda: '30 Mbps', winbox: '', costo: 1742.40 },
        { id_tipo: 4, ancho_banda: '35 Mbps', winbox: '', costo: 2032.80 },
        { id_tipo: 4, ancho_banda: '40 Mbps', winbox: '', costo: 2323.20 },
        { id_tipo: 4, ancho_banda: '45 Mbps', winbox: '', costo: 2613.60 },
        { id_tipo: 4, ancho_banda: '50 Mbps', winbox: '', costo: 2904.0 },
        { id_tipo: 4, ancho_banda: '55 Mbps', winbox: '', costo: 3194.40 },
        { id_tipo: 4, ancho_banda: '60 Mbps', winbox: '', costo: 3484.80 },
        { id_tipo: 4, ancho_banda: '65 Mbps', winbox: '', costo: 3775.20 },
        { id_tipo: 4, ancho_banda: '70 Mbps', winbox: '', costo: 4065.60 },
        { id_tipo: 4, ancho_banda: '75 Mbps', winbox: '', costo: 4356.0 },
        { id_tipo: 4, ancho_banda: '80 Mbps', winbox: '', costo: 4646.40 },
        { id_tipo: 4, ancho_banda: '85 Mbps', winbox: '', costo: 4936.80 },
        { id_tipo: 4, ancho_banda: '90 Mbps', winbox: '', costo: 5227.20 },
        { id_tipo: 4, ancho_banda: '95 Mbps', winbox: '', costo: 5517.60 },
        { id_tipo: 4, ancho_banda: '100 Mbps', winbox: '', costo: 5808.0 },
        // Nateado
        { id_tipo: 5, ancho_banda: '10 Mbps', winbox: '', costo: 493.68 },
        { id_tipo: 5, ancho_banda: '15 Mbps', winbox: '', costo: 740.52 },
        { id_tipo: 5, ancho_banda: '20 Mbps', winbox: '', costo: 987.36 },
        { id_tipo: 5, ancho_banda: '25 Mbps', winbox: '', costo: 1234.20 },
        { id_tipo: 5, ancho_banda: '30 Mbps', winbox: '', costo: 1481.04 },
        { id_tipo: 5, ancho_banda: '35 Mbps', winbox: '', costo: 1727.88 },
        { id_tipo: 5, ancho_banda: '40 Mbps', winbox: '', costo: 1974.72 },
        { id_tipo: 5, ancho_banda: '45 Mbps', winbox: '', costo: 2221.56 },
        { id_tipo: 5, ancho_banda: '50 Mbps', winbox: '', costo: 2468.40 },
        { id_tipo: 5, ancho_banda: '55 Mbps', winbox: '', costo: 2715.24 },
        { id_tipo: 5, ancho_banda: '60 Mbps', winbox: '', costo: 2962.08 },
        { id_tipo: 5, ancho_banda: '65 Mbps', winbox: '', costo: 3208.92 },
        { id_tipo: 5, ancho_banda: '70 Mbps', winbox: '', costo: 3455.76 },
        { id_tipo: 5, ancho_banda: '75 Mbps', winbox: '', costo: 3702.60 },
        { id_tipo: 5, ancho_banda: '80 Mbps', winbox: '', costo: 3949.44 },
        { id_tipo: 5, ancho_banda: '85 Mbps', winbox: '', costo: 4196.28 },
        { id_tipo: 5, ancho_banda: '90 Mbps', winbox: '', costo: 4443.12 },
        { id_tipo: 5, ancho_banda: '95 Mbps', winbox: '', costo: 4689.96 },
        { id_tipo: 5, ancho_banda: '100 Mbps', winbox: '', costo: 4936.80 },
        // Comerciales
        { id_tipo: 6, ancho_banda: '1 Mbps', winbox: '', costo: 20.0 },
        { id_tipo: 6, ancho_banda: '2 Mbps', winbox: '', costo: 40.0 },
        { id_tipo: 6, ancho_banda: '3 Mbps', winbox: '', costo: 60.0 },
        { id_tipo: 6, ancho_banda: '4 Mbps', winbox: '', costo: 79.0 },
        { id_tipo: 6, ancho_banda: '5 Mbps', winbox: '', costo: 99.0 },
        { id_tipo: 6, ancho_banda: '6 Mbps', winbox: '', costo: 118.0 },
        { id_tipo: 6, ancho_banda: '7 Mbps', winbox: '', costo: 138.0 },
        { id_tipo: 6, ancho_banda: '8 Mbps', winbox: '', costo: 157.0 },
        { id_tipo: 6, ancho_banda: '9 Mbps', winbox: '', costo: 176.0 },
        { id_tipo: 6, ancho_banda: '10 Mbps', winbox: '', costo: 195.0 },
        { id_tipo: 5, ancho_banda: '15 Mbps', winbox: '', costo: 289.0 },
        { id_tipo: 5, ancho_banda: '20 Mbps', winbox: '', costo: 380.0 },
        { id_tipo: 5, ancho_banda: '25 Mbps', winbox: '', costo: 469.0 },
        { id_tipo: 5, ancho_banda: '30 Mbps', winbox: '', costo: 555.0 },
        { id_tipo: 5, ancho_banda: '35 Mbps', winbox: '', costo: 639.0 },
        { id_tipo: 5, ancho_banda: '40 Mbps', winbox: '', costo: 720.0 },
        { id_tipo: 5, ancho_banda: '45 Mbps', winbox: '', costo: 799.0 },
        { id_tipo: 5, ancho_banda: '50 Mbps', winbox: '', costo: 875.0 }
      ])
    })
}
