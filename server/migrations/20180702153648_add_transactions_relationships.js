exports.up = function (knex, Promise) {
  return knex.schema.table('detalles_movimientos', function (t) {
    t.integer('id_movimiento').unsigned().references('movimientos.id')
      .onDelete('cascade')
    t.integer('id_servicio').unsigned().references('servicios.id')
      .onDelete('cascade')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('detalles_movimientos', function (t) {
    t.dropForeign(['id_movimiento', 'id_servicio'])
    t.dropColumns('id_movimiento', 'id_servicio')
  })
}
