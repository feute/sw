exports.up = function (knex, Promise) {
  return knex.schema.createTable('clientes', function (t) {
    t.increments('id').unsigned().primary()
    t.string('nombre')
    t.string('correo1')
    t.string('correo2')
    t.string('status')
    t.string('cedula')
    t.string('rif')
    t.string('compania')
    t.string('cargo')
    t.string('direccion')
    t.string('pais')
    t.string('ciudad')
    t.string('telefono1')
    t.string('telefono2')
    t.string('fax')
    t.string('observaciones')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('clientes')
}
