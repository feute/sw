exports.up = function (knex, Promise) {
  return knex.schema.createTable('proveedores', function (t) {
    t.increments('id').unsigned().primary()
    t.string('nombre_compania')
    t.string('nombre_contacto')
    t.string('cargo_contacto')
    t.string('direccion')
    t.string('ciudad')
    t.string('region')
    t.integer('codigo_postal')
    t.string('pais')
    t.string('telefono')
    t.string('fax')
    t.string('pagina_principal')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('proveedores')
}
