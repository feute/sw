exports.up = function (knex, Promise) {
  return knex.schema.table('clientes', function (t) {
    t.dropForeign('id_nodo')
    t.dropColumn('id_nodo')
    t.dropColumn('plan')
    t.dropColumn('status')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('clientes', function (t) {
    t.string('plan')
    t.string('status')
    t.integer('id_nodo').unsigned().references('nodos.id')
      .onDelete('cascade')
  })
}
