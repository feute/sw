exports.up = function (knex, Promise) {
  return knex.schema.table('planes', function (t) {
    t.integer('id_tipo').unsigned().references('tipos_planes.id')
      .onDelete('cascade')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('planes', function (t) {
    t.dropForeign('id_tipo')
    t.dropColumn('id_tipo')
  })
}
