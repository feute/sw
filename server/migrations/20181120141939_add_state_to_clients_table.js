exports.up = function (knex, Promise) {
  return knex.schema.table('clientes', function (t) {
    t.string('estado')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('clientes', function (t) {
    t.dropColumn('estado')
  })
}
