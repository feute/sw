exports.up = function (knex, Promise) {
  return knex.schema.createTable('formas_pago', function (t) {
    t.increments('id').unsigned().primary()
    t.string('nombre_compania')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('formas_pago')
}
