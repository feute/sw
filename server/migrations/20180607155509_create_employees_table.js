exports.up = function (knex, Promise) {
  return knex.schema.createTable('empleados', function (t) {
    t.increments('id').unsigned().primary()
    t.string('usuario')
    t.string('clave')
    t.string('nombres')
    t.string('apellidos')
    t.string('cedula')
    t.string('rif')
    t.string('ficha')
    t.string('tratamiento')
    t.string('cargo')
    t.date('fecha_nacimiento')
    t.date('fecha_contratacion')
    t.string('direccion')
    t.string('ciudad')
    t.string('region')
    t.integer('codigo_postal')
    t.string('pais')
    t.string('telefono_domicilio')
    t.string('telefono_celular')
    t.string('foto')
    t.string('notas')
    t.string('supervisor')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('empleados')
}
