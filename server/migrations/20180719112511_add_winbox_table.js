exports.up = function (knex, Promise) {
  return knex.schema.createTable('winbox', function (t) {
    t.increments('id').unsigned().primary()
    t.integer('id_cliente').unsigned().references('clientes.id')
      .onDelete('cascade')
    t.string('name')
    t.string('password')
    t.string('server')
    t.string('mac')
    t.string('address')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('winbox')
}
