exports.up = function (knex, Promise) {
  return knex.schema.createTable('tipos_planes', function (t) {
    t.increments('id').unsigned().primary()
    t.string('nombre')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('tipos_planes')
}
