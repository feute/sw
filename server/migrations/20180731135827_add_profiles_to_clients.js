exports.up = function (knex, Promise) {
  return knex.schema.table('clientes', function (t) {
    t.string('plan')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('clientes', function (t) {
    t.dropColumn('plan')
  })
}
