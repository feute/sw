exports.up = function (knex, Promise) {
  return knex.schema.createTable('movimientos', function (t) {
    t.increments('id').unsigned().primary()
    t.date('fecha_movimiento')
    t.date('fecha_entrega')
    t.date('fecha_envio')
    t.string('forma_envio')
    t.string('cargo_envio')
    t.string('nombre_pagador')
    t.string('direccion_pagador')
    t.string('ciudad_pagador')
    t.string('estado_pagador')
    t.string('pais_pagador')
    t.string('rif_pagador')
    t.string('nit_pagador')
    t.string('telefono_pagador')
    t.string('fax_pagador')
    t.string('correo_pagador')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('movimientos')
}
