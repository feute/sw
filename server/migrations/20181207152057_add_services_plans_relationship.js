exports.up = function (knex, Promise) {
  return knex.schema.table('servicios', function (t) {
    t.integer('id_plan').unsigned().references('planes.id')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('servicios', function (t) {
    t.dropForeign('id_plan')
    t.dropColumn('id_plan')
  })
}
