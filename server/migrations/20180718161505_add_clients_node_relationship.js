exports.up = function (knex, Promise) {
  return knex.schema.table('clientes', function (t) {
    t.integer('id_nodo').unsigned().references('nodos.id')
      .onDelete('cascade')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('clientes', function (t) {
    t.dropForeign('id_nodo')
    t.dropColumn('id_nodo')
  })
}
