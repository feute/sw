exports.up = function (knex, Promise) {
  return knex.schema.table('movimientos', function (t) {
    t.integer('id_cliente').unsigned().references('clientes.id')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('clientes', function (t) {
    t.dropForeign('id_cliente')
    t.dropColumn('id_cliente')
  })
}
