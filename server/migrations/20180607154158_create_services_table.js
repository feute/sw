exports.up = function (knex, Promise) {
  return knex.schema.createTable('servicios', function (t) {
    t.increments('id').unsigned().primary()
    t.string('plan')
    t.string('status')
    t.string('name')
    t.string('password')
    t.string('server')
    t.string('mac')
    t.string('address')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('servicios')
}
