exports.up = function (knex, Promise) {
  return knex.schema.createTable('planes', function (t) {
    t.increments('id').unsigned().primary()
    t.string('ancho_banda')
    t.string('winbox') // Nombre del plan en Winbox.
    t.float('costo')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('planes')
}
