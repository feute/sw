exports.up = function (knex, Promise) {
  return knex.schema.table('tipos_planes', function (t) {
    t.string('prefijo') // Prefijo del plan en el Winbox
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('tipos_planes', function (t) {
    t.dropColumn('prefijo')
  })
}
