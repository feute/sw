exports.up = function (knex, Promise) {
  return knex.schema.createTable('nodos', function (t) {
    t.increments('id').unsigned().primary()
    t.string('nombre')
    t.string('ip')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('nodos')
}
