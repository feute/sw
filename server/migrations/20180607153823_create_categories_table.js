exports.up = function (knex, Promise) {
  return knex.schema.createTable('categorias', function (t) {
    t.increments('id').unsigned().primary()
    t.string('nombre')
    t.string('descripcion')
    t.string('imagen')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('categorias')
}
