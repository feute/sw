exports.up = function (knex, Promise) {
  return knex.schema.table('servicios', function (t) {
    t.integer('id_cliente').unsigned().references('clientes.id')
      .onDelete('cascade')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.table('servicios', function (t) {
    t.dropForeign('id_cliente')
    t.dropColumn('id_cliente')
  })
}
