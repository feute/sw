exports.up = function (knex, Promise) {
  return knex.schema.createTable('detalles_movimientos', function (t) {
    t.increments('id').unsigned().primary()
    t.integer('cantidades_vendidas')
    t.integer('cantidades_recibidas')
    t.integer('cantidades_stock')
    t.float('precio_unidad')
    t.float('descuento')
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('detalles_movimientos')
}
