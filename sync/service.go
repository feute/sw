package main

import (
	"database/sql"
	"fmt"
)

type Service struct {
	IDNodo    int64
	IDCliente int64
	IDPlan    int64
	Status    string
	Name      string
	Password  string
	Server    string
	MAC       string
	Address   string
}

func (db *myDB) AddService(s *Service) (id int64, err error) {
	planID := sql.NullInt64{}
	if s.IDPlan > 0 {
		planID = sql.NullInt64{s.IDPlan, true}
	}
	r, err := execStmt(db.insertService, s.IDNodo, s.IDCliente,
		planID, s.Status, s.Name, s.Password, s.Server,
		s.MAC, s.Address)
	if err != nil {
		return 0, err
	}

	lastInsertID, err := r.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("mysql: could not get last insert ID: %v", err)
	}

	return lastInsertID, nil
}

func (db *myDB) ListServices() ([]*Service, error) {
	rows, err := db.listServices.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var services []*Service
	for rows.Next() {
		var name string
		if err = rows.Scan(&name); err != nil {
			return nil, err
		}

		service := &Service{Name: name}
		services = append(services, service)
	}
	return services, nil
}
