// The structure of this file and database handling is taken from the
// Bookshelf golang sample, by the Google Cloud Platform team/community.
// URL: https://github.com/GoogleCloudPlatform/golang-samples/blob/master/getting-started/bookshelf/db_mysql.go
package main

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

type myDB struct {
	conn *sql.DB

	listNodes     *sql.Stmt
	listPlans     *sql.Stmt
	listServices  *sql.Stmt
	insertClient  *sql.Stmt
	insertService *sql.Stmt
}

var _ SWDatabase = &myDB{}

// DBConfig represents the database parameters for a connection.
type DBConfig struct {
	Username, Password string
	Host               string
	Port               int
}

type SWDatabase interface {
	ListNodes() ([]*Node, error)
	ListPlans() ([]*Plan, error)
	ListServices() ([]*Service, error)
	AddClient(*Client) (int64, error)
	AddService(*Service) (int64, error)

	Close()
}

func (db *myDB) Close() {
	db.conn.Close()
}

func (c DBConfig) dataStoreName(databaseName string) string {
	var cred string
	// [username[:password]@]
	if c.Username != "" {
		cred = c.Username
		if c.Password != "" {
			cred = cred + ":" + c.Password
		}
		cred = cred + "@"
	}

	if c.Host == "" {
		c.Host = "127.0.0.1"
	}

	if c.Port == 0 {
		c.Port = 3306
	}

	return fmt.Sprintf("%stcp([%s]:%d)/%s", cred, c.Host, c.Port, databaseName)
}

const listNodesStmt = `SELECT id, nombre, ip FROM nodos`
const listServicesStmt = `SELECT name FROM servicios`
const listPlansStmt = `SELECT p.id, t.id AS t_id, t.prefijo, p.ancho_banda FROM
	planes AS p
	JOIN tipos_planes AS t
	ON p.id_tipo = t.id`
const insertClientStmt = `INSERT INTO clientes (nombre, correo1)
	VALUES (?, ?)`
const insertServiceStmt = `INSERT INTO servicios
	(id_nodo, id_cliente, id_plan, status, name, password, server, mac, address)
	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`

func newDB(config DBConfig) (SWDatabase, error) {
	var dbName = os.Getenv("DB_NAME")
	if dbName == "" {
		dbName = "sw"
	}

	conn, err := sql.Open("mysql", config.dataStoreName(dbName))
	if err != nil {
		return nil, fmt.Errorf("mysql connection error: %v", err)
	}
	if err := conn.Ping(); err != nil {
		conn.Close()
		return nil, fmt.Errorf("could not establish good connection to db: %v", err)
	}

	db := &myDB{
		conn: conn,
	}

	if db.listNodes, err = conn.Prepare(listNodesStmt); err != nil {
		return nil, fmt.Errorf("mysql: prepare list: %v", err)
	}

	if db.listPlans, err = conn.Prepare(listPlansStmt); err != nil {
		return nil, fmt.Errorf("mysql: prepare list: %v", err)
	}

	if db.listServices, err = conn.Prepare(listServicesStmt); err != nil {
		return nil, fmt.Errorf("mysql: prepare list: %v", err)
	}

	if db.insertClient, err = conn.Prepare(insertClientStmt); err != nil {
		return nil, fmt.Errorf("mysql: prepare insert: %v", err)
	}

	if db.insertService, err = conn.Prepare(insertServiceStmt); err != nil {
		return nil, fmt.Errorf("mysql: prepare insert: %v", err)
	}

	return db, nil
}

// execStmt executes a given statement, expecting one row to be affected.
func execStmt(stmt *sql.Stmt, args ...interface{}) (sql.Result, error) {
	r, err := stmt.Exec(args...)
	if err != nil {
		return r, fmt.Errorf("mysql: could not execute statement: %v", err)
	}
	rowsAffected, err := r.RowsAffected()
	if err != nil {
		return r, fmt.Errorf("mysql: could not get rows affected: %v", err)
	} else if rowsAffected != 1 {
		return r, fmt.Errorf("mysql: expected 1 row affected, got %d", rowsAffected)
	}
	return r, nil
}
