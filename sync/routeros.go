package main

import (
	"log"
	"strings"

	routeros "gopkg.in/routeros.v2"
)

func findNameInService(services []*Service, name string) bool {
	for _, service := range services {
		if service.Name == name {
			return true
		}
	}

	return false
}

func syncClients(nodeID int64, c *routeros.Client) (int, error) {
	var err error
	var counter = 0

	r, err := c.RunArgs([]string{"/ip/hotspot/user/print"})
	if err != nil {
		return 0, err
	}

	services, err := db.ListServices()
	if err != nil {
		return 0, nil
	}

	plans, err := db.ListPlans()
	if err != nil {
		return 0, nil
	}

	for _, user := range r.Re {
		var planID int64
		var status = "Activo"

		if strings.Contains(user.Map["comment"], "trial user") {
			continue
		}

		// Skip user if it's already found in the database.
		if findNameInService(services, user.Map["name"]) {
			continue
		}

		if user.Map["profile"] == "Suspendido" ||
			user.Map["profile"] == "Advertencia" {
			// {
			status = "Suspendido"
		}

		// Get the plan ID by comparing it to profile field
		// in Winbox.
		for _, plan := range plans {
			if plan.Name() == user.Map["profile"] {
				planID = plan.ID
			}
		}

		// Add user to db
		client := &Client{user.Map["comment"], user.Map["email"]}
		clientID, err := db.AddClient(client)

		if clientID > 0 {
			service := &Service{
				IDNodo:    nodeID,
				IDCliente: clientID,
				IDPlan:    planID,
				Status:    status,
				Name:      user.Map["name"],
				Password:  user.Map["password"],
				Server:    user.Map["server"],
				MAC:       user.Map["mac-address"],
				Address:   user.Map["address"],
			}
			_, err = db.AddService(service)
			if err != nil {
				log.Print(err)
			}
		}
		if err == nil {
			counter += 1
		}
	}

	return counter, err
}
