package main

import "fmt"

type Client struct {
	Nombre string
	Correo string
}

func (db *myDB) AddClient(c *Client) (id int64, err error) {
	r, err := execStmt(db.insertClient, c.Nombre, c.Correo)
	if err != nil {
		return 0, err
	}

	lastInsertID, err := r.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("mysql: could not get last insert ID: %v", err)
	}

	return lastInsertID, nil
}
