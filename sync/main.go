package main

import (
	"log"
	"os"
	"time"

	"github.com/joho/godotenv"
	routeros "gopkg.in/routeros.v2"
)

const apiPort = "8728"

var username, password string
var db SWDatabase

func dial(address string) (*routeros.Client, error) {
	return routeros.Dial(address+":"+apiPort, username, password)
}

func handleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	err := godotenv.Load()
	handleError(err)
	username = os.Getenv("WINBOX_USER")
	password = os.Getenv("WINBOX_PASS")

	dbConfig := DBConfig{
		Username: os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASS"),
		Host:     os.Getenv("DB_HOST"),
	}
	db, err = newDB(dbConfig)
	handleError(err)
	defer db.Close()

	log.Println("starting syncing process")
	for {
		nodes, err := db.ListNodes()
		handleError(err)

		for _, node := range nodes {
			c, err := dial(node.IP)
			handleError(err)

			// Handle connection `c`.
			counter, err := syncClients(node.ID, c)
			if err != nil {
				handleError(err)
			}
			log.Printf("%d clientes agregados desde el nodo %s",
				counter,
				node.Name,
			)
			c.Close()
		}

		log.Println("---")
		time.Sleep(5 * time.Second)
	}
}
