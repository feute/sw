package main

type Plan struct {
	ID         int64
	TypeID     int64 // Plan type ID.
	Prefijo    string
	AnchoBanda string
}

func (p *Plan) Name() string {
	return p.Prefijo + " " + p.AnchoBanda
}

func (db *myDB) ListPlans() ([]*Plan, error) {
	rows, err := db.listPlans.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var plans []*Plan
	for rows.Next() {
		var (
			id         int64
			typeID     int64
			prefijo    string
			anchoBanda string
		)
		if err = rows.Scan(&id, &typeID, &prefijo, &anchoBanda); err != nil {
			return nil, err
		}

		plan := &Plan{id, typeID, prefijo, anchoBanda}
		plans = append(plans, plan)
	}
	return plans, nil
}
