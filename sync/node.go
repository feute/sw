package main

// Node represents a single RouterOS server.
type Node struct {
	ID   int64
	Name string
	IP   string
}

// ListNodes fetches the nodes from the database and return a slice of
// nodes.
func (db *myDB) ListNodes() ([]*Node, error) {
	rows, err := db.listNodes.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var nodes []*Node
	for rows.Next() {
		var id int64
		var name, ip string
		if err = rows.Scan(&id, &name, &ip); err != nil {
			return nil, err
		}

		node := &Node{id, name, ip}
		nodes = append(nodes, node)
	}
	return nodes, nil
}
